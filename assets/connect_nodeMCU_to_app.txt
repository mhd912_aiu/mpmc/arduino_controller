#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>

const char* ssid = "NodeMCU";
const char* pass = "12345678";

#define ledPin D1                     // Pin for LED
#define ONE_WIRE_BUS D3               // Pin for DS18B20 data wire
OneWire oneWire(ONE_WIRE_BUS);        // OneWire instance
DallasTemperature sensors(&oneWire);  // DallasTemperature instance

String json;
char tstr[10];
char hstr[10];
char histr[10];

WebSocketsServer webSocket = WebSocketsServer(81);

unsigned long lastTempSentTime = 0;              // Track the last time the temperature was sent
const unsigned long tempSendingInterval = 2000;  // Interval for sending temperature (2 seconds)

void webSocketEvent(uint8_t num, WStype_t type, uint8_t* payload, size_t length) {
  String cmd = "";
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.println("Websocket is disconnected");
      break;
    case WStype_CONNECTED:
      Serial.println("Websocket is connected");
      Serial.println(webSocket.remoteIP(num).toString());
      webSocket.sendTXT(num, "connected");
      break;
    case WStype_TEXT:
      cmd = "";
      for (int i = 0; i < length; i++) {
        cmd += (char)payload[i];
      }
      Serial.println(cmd);

      if (cmd == "readdata") {
        // Read temperature from DS18B20 sensor
        sensors.requestTemperatures();
        float t = sensors.getTempCByIndex(0);

        if (t == DEVICE_DISCONNECTED_C) {
          Serial.println("Failed to read from DS18B20 sensor!");
        } else {
          sprintf(tstr, "%.2f", t);
          json = "{'temp':'" + String(tstr) + "'}";
          Serial.println("DS18B20 Data read successful");
          webSocket.broadcastTXT(json);
        }
      } else if (cmd == "ledOn") {
        digitalWrite(ledPin, HIGH);  // Turn on LED
      } else if (cmd == "ledOff") {
        digitalWrite(ledPin, LOW);  // Turn off LED
      }

      webSocket.sendTXT(num, cmd + ":success");

      break;
    default:
      break;
  }
}

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);

  Serial.println("Connecting to WiFi");
  IPAddress apIP(192, 168, 0, 1);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssid, pass);

  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  Serial.println("Websocket is started");

  sensors.begin();
}

void loop() {
  webSocket.loop();

  unsigned long currentMillis = millis();

  // Check if it's time to send temperature reading
  if (currentMillis - lastTempSentTime >= tempSendingInterval) {
    // Read temperature from DS18B20 sensor
    sensors.requestTemperatures();
    float t = sensors.getTempCByIndex(0);

    if (t == DEVICE_DISCONNECTED_C) {
      Serial.println("Failed to read from DS18B20 sensor!");
    } else {
      sprintf(tstr, "%.2f", t);
      json = "{'temp':'" + String(tstr) + "'}";
      Serial.println("DS18B20 Data read successful");
      webSocket.broadcastTXT(json);
    }

    lastTempSentTime = currentMillis;  // Update the last sent time
  }

  // Process any other events or tasks here
  // ...
}
